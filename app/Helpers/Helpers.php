<?php

if (!function_exists('set_response')) {
    function set_response($message, $code, $data = [])
    {
        if (empty($data)) {
            return response(["status_code" => $code, "message" => $message]);
        }
        return response(
            ["status_code" => $code, "message" => $message, "data" => $data]
        );
    }
}
