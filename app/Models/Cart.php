<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ["user_id", "product_id", "quantity"];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function defaultView()
    {
        return [
            "user_id" => $this->user_id,
            "quantity" => $this->quantity,
            "product" => $this->product
        ];
    }
}
