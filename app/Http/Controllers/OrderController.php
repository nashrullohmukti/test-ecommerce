<?php

namespace App\Http\Controllers;

use App\Interfaces\CartInterface;
use App\Interfaces\OrderInterface;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $repository, $data_key, $data_keys, $user, $cart_repository;

    public function __construct(OrderInterface $repository, CartInterface $cart_repository)
    {
        $this->repository = $repository;
        $this->data_key = "order";
        $this->data_keys = "orders";
        $this->cart_repository = $cart_repository;
    }

    public function store(Request $request)
    {
        foreach (request()->cart_ids as $cart_id) {
            $cart = $this->cart_repository->show($cart_id);
            if ($cart["quantity"] > $cart["product"]["stock"]) {
                set_response("Product " . $cart->product->name . " stock is unavailable", Response::HTTP_OK);
            }
        }
        $data = $this->repository->store($request);
        return set_response("Order created successfully", Response::HTTP_CREATED, [$this->data_key => $data]);
    }
}
