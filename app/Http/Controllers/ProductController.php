<?php

namespace App\Http\Controllers;

use App\Interfaces\ProductInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    protected $repository, $data_key, $data_keys;

    public function __construct(ProductInterface $repository)
    {
        $this->repository = $repository;
        $this->data_key = "product";
        $this->data_keys = "products";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->repository->list($request);
        return set_response("Get product data successfully", Response::HTTP_OK, [$this->date_keys => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return set_response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $data = $this->repository->store($request);
        return set_response("Product saved successfully", Response::HTTP_CREATED, [$this->data_key => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->repository->show($id);
        if (is_string($data)) {
            return set_response($data, Response::HTTP_NOT_FOUND);
        }
        return set_response("Get product data successfully", Response::HTTP_OK, [$this->data_key => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return set_response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $data = $this->repository->update($request, $id);
        if (is_string($data)) {
            return set_response($data, Response::HTTP_NOT_FOUND);
        }

        return set_response("Product updated successfully", Response::HTTP_OK, [$this->data_key => $data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroy($id);
        return set_response("Product deleted successfully", Response::HTTP_OK);
    }
}
