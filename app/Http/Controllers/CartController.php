<?php

namespace App\Http\Controllers;

use App\Interfaces\CartInterface;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $repository, $data_key, $data_keys, $user;

    public function __construct(CartInterface $repository)
    {
        $this->repository = $repository;
        $this->data_key = "cart";
        $this->data_keys = "carts";
    }

    public function showMyCart()
    {
        $data = $this->repository->showMyCart();
        if (!$data->count()) {
            return set_response("Cart is empty!", Response::HTTP_OK);
        }
        return set_response("Get cart data successfully", Response::HTTP_OK, [$this->data_keys => $data]);
    }

    public function addProduct(Request $request)
    {
        $this->repository->addProduct($request);
        return set_response("Cart successfully changed!", Response::HTTP_OK);
    }

    public function deleteProduct($product_id)
    {
        $this->repository->deleteProduct($product_id);
        return set_response("Product successfully deleted from cart!", Response::HTTP_OK);
    }
}
