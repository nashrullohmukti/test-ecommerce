<?php

namespace App\Repositories;

use App\Models\Product;
use App\Interfaces\ProductInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ProductRepository implements ProductInterface
{
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function list($request)
    {
        return $this->model->active()->get();
    }

    public function show($id)
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function store(Request $request)
    {
        return $this->model->create($request->toArray());
    }

    public function update(Request $request, $id)
    {
        $product = $this->model->find($id);
        $product->slug = null;
        $product->update($request->toArray());
        return $product;
    }

    public function destroy($id)
    {
        return $this->model->find($id)->delete();
    }
}
