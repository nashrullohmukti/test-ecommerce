<?php

namespace App\Repositories;

use App\Interfaces\CartInterface;
use App\Models\Order;
use App\Interfaces\OrderInterface;
use App\Models\Cart;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderRepository implements OrderInterface
{
    protected $model, $relations, $cart_repository, $order_product_model;

    public function __construct(Order $model, CartInterface $cart_repository, OrderProduct $order_product_model, Cart $cart_model)
    {
        $this->model = $model;
        $this->relations = ["products"];
        $this->cart_repository = $cart_repository;
        $this->order_product_model = $order_product_model;
        $this->cart_model = $cart_model;
    }

    public function store(Request $request)
    {
        $total_price = 0;
        $order = $this->model->create([
            "user_id" => Auth::user()->id,
            "status" => "created",
            "total_price" => $total_price,
            "notes" => request()->notes ?? ""
        ]);

        foreach (request()->cart_ids as $cart_id) {
            $cart = $this->cart_repository->show($cart_id);
            $this->order_product_model->create([
                "order_id" => $order->id,
                "product_id" => $cart["product"]["id"],
                "quantity" => $cart["quantity"]
            ]);
            $total_price += ($cart["product"]["price"] * $cart["quantity"]);
        }

        $order->total_price = $total_price;
        $order->update();
        $this->cart_model->whereIn("id", request()->cart_ids)->delete();
        return $order;
    }
}
