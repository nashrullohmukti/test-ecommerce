<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Interfaces\CartInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartRepository implements CartInterface
{
    protected $model, $relations;

    public function __construct(Cart $model)
    {
        $this->model = $model;
        $this->relations = ["product"];
    }

    public function show($id)
    {
        return $this->model->with($this->relations)->where("id", $id)->first()->defaultView();
    }
    public function showMyCart()
    {
        $carts = $this->model->with($this->relations)->where("user_id", Auth::user()->id)->get()->map->defaultView();
        return $carts ?? [];
    }

    public function addProduct(Request $request)
    {
        return $this->model->updateOrCreate(
            ["user_id" =>  Auth::user()->id, "product_id" => request()->product_id],
            ["quantity" => request()->quantity]
        );
    }

    public function deleteProduct($product_id)
    {
        return $this->model->where(
            ["user_id" =>  Auth::user()->id, "product_id" => $product_id]
        )->delete();
    }
}
