<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface CartInterface
{
    public function show($id);
    public function showMyCart();
    public function addProduct(Request $request);
    public function deleteProduct($product_id);
}
