## Task 1 : Online Store

Problems:

1. Describe what you think happened that caused those bad reviews during our 12.12 event and why it happened.

    - For inventory quantities misreported : This happens because the system does not limit the quantity data so as not to be minus. Besides that, the absence of a system that checks for the quantity of products available at the time of making an order can also lead to a minus stock situation.
    - For bad reviews: I think this happened because the user did not get the service properly. Because the user feels that he has paid for his order and it turns out that the cancellation process occurs a few days later.

2. Based on your analysis, propose a solution that will prevent the incidents from occurring again.

    - The warehouse admin should do a stock take periodically.
    - Create a system with a stock value that is always positive value (not less than zero).
    - Create a system by checking the available quantity before making an order.
    - Create a system that cancels orders directly so that users don't receive cancellations a few days later.

3. Based on your proposed solution, build a Proof of Concept that demonstrates technically how your solution will work.
    - I created system that quantity data is always positive value.
    - If users order quantity is higher than available quantity, user cant making an order.

## Minimum Requirements

This project has a few system requirements. You should ensure that your web server has the following minimum PHP version and extensions:

-   Composer >= 1.0.0
-   PHP >= 7.3
-   PostgreSQL > 12.8
-   BCMath PHP Extension
-   Ctype PHP Extension
-   Fileinfo PHP Extension
-   JSON PHP Extension
-   Mbstring PHP Extension
-   OpenSSL PHP Extension
-   PDO PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension

## Run The Project

1. Clone this priject from https://gitlab.com/nashrullohmukti/test-ecommerce
2. Edit your local .env file
3. Go to project folder
4. Run "composer install"
5. Run "php artisan key:generate"
6. Run "php artisan migrate"
7. Run "php artisan passport:install"
8. Run "php artisan db:seed"
9. Run "php artisan serve"

## Run Unit Testing

1. Run "php artisan test"

## Run Endpoints

Try this API collection for testing endpoints from this project https://www.getpostman.com/collections/b3db71a1dd9c68299806
