import os, random, sys,tty,termios


maps = [
    ["#","#","#","#","#","#","#","#"], 
    ["#", 0 , 0 , 0 , 0 , 0 , 0 ,"#"], 
    ["#", 0 ,"#","#","#", 0 , 0 ,"#"], 
    ["#", 0 , 0 , 0 ,"#", 0 ,"#","#"], 
    ["#","X","#", 0 , 0 , 0 , 0 ,"#"], 
    ["#","#","#","#","#","#","#","#"]
];
result = 0

class _Getch:
    def __call__(self):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(3)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

def treasure_rand():
    for x in range(len(maps)):
        for y in range(len(maps[x])):
            if maps[x][y] == 0:
                maps[x][y] = random.choice([2, 1, 1, 1, 1, 1, 0, 0, 0, 0])
                if maps[x][y] == 2:
                    break
        else:
            continue
        break
    treasure_availability()

def treasure_availability():
    available = 0
    for x in range(len(maps)):
        for y in range(len(maps[x])):
            if maps[x][y] == 2:
                available = 1
    if not available:                
        treasure_rand()

def treasure_check():
    if result == 1:
        print("You're great!")
    else:
        print("Please find the Treasure!")

def treasure_maps():
    os.system("clear")
    for x in maps:
        for y in x:
            if y == 2 or y == 1:
                print("$", end=" ")
            elif y == 0:
                print(".", end=" ")
            else:
                if y == "X" or y == "$": 
                    print(y, end=" ")
                else:
                    print("#", end=" ")
        print("\n")
    treasure_check()

def reset_maps():
    for x in range(len(maps)):
        for y in range(len(maps[x])):
            if maps[x][y] == 1:
                maps[x][y] = 0
    treasure_maps()

def change_key(val):
    global result
    for x in range(len(maps)):
        for y in range(len(maps[x])):
            if maps[x][y] == "X":
                if val == "up":
                    if maps[x-1][y] != "#":
                        if maps[x-1][y] != 2:
                            maps[x][y] = 0
                            maps[x-1][y] = "X"
                            break
                        else:
                            maps[x][y] = 0
                            maps[x-1][y] = "$"
                            result = 1
                elif val == "bottom":
                    if maps[x+1][y] != "#":
                        if maps[x+1][y] != 2:
                            maps[x][y] = 0
                            maps[x+1][y] = "X"
                            break
                        else:
                            maps[x][y] = 0
                            maps[x+1][y] = "$"
                            result = 1
                elif val == "right":
                    if maps[x][y+1] != "#":
                        if maps[x][y+1] != 2:
                            maps[x][y] = 0
                            maps[x][y+1] = "X"
                            break
                        else:
                            maps[x][y] = 0
                            maps[x][y+1] = "$"
                            result = 1
                elif val == "left":
                    if maps[x][y-1] != "#":
                        if maps[x][y-1] != 1:
                            maps[x][y] = 0
                            maps[x][y-1] = "X"
                            break
                        else:
                            maps[x][y] = 0
                            maps[x][y-1] = "$"
                            result = 1
        else:
            continue
        break

def pressed_key():
    inkey = _Getch()
    while(1):
        k=inkey()
        if k=='\x1b[A': change_key("up")
        elif k=='\x1b[B': change_key("bottom")
        elif k=='\x1b[C': change_key("right")
        elif k=='\x1b[D': change_key("left")
        if k != "" : break

def main():
    treasure_rand()
    while result == 0:
        treasure_maps()
        pressed_key()
    
    if result == 1:
        reset_maps()

main()
