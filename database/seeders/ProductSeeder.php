<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                "name" => "Iphone 7",
                "slug" => "iphone-7",
                "description" => "This is Iphone 7",
                "stock" => 10,
                "price" => 3500000,
                "active" => true,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "name" => "Iphone 8",
                "slug" => "iphone-8",
                "description" => "This is Iphone 8",
                "stock" => 10,
                "price" => 5000000,
                "active" => true,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "name" => "Iphone 8 Plus",
                "slug" => "iphone-8-plus",
                "description" => "This is Iphone 8 Plus",
                "stock" => 10,
                "price" => 5500000,
                "active" => true,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "name" => "Iphone X",
                "slug" => "iphone-x",
                "description" => "This is Iphone X",
                "stock" => 10,
                "price" => 600000,
                "active" => true,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "name" => "Iphone XR",
                "slug" => "iphone-xr",
                "description" => "This is Iphone XR",
                "stock" => 10,
                "price" => 6500000,
                "active" => true,
                "created_at" => now(),
                "updated_at" => now()
            ]
        ];
        Product::insert($products);
    }
}
