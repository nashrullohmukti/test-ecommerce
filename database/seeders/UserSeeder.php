<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin', "guard_name" => "web"
            ],
            [
                'name' => 'customer', "guard_name" => "web"
            ]
        ];
        Role::insert($roles);

        $user = User::create([
            "name" => "Administrator",
            "email" => "admin@email.test",
            "email_verified_at" => now(),
            "password" => bcrypt("admin")
        ]);
        $user->assignRole('admin');

        $user = User::create([
            "name" => "Customer",
            "email" => "customer@email.test",
            "email_verified_at" => now(),
            "password" => bcrypt("customer")
        ]);
        $user->assignRole('customer');
    }
}
