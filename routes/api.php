<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post("login", [AuthController::class, "login"])->name("login");
Route::post("register", [AuthController::class, "register"])->name("register");
Route::resource("products", ProductController::class)->only(["index", "show"]);

Route::middleware(["auth:api"])->group(function () {
    Route::get("logout", [AuthController::class, "logout"])->name("logout");
    Route::middleware(["role:admin"])->group(function () {
        Route::resource("products", ProductController::class)->only(["store", "update", "destroy"]);
    });

    Route::group(["prefix" => "carts"], function () {
        Route::get("my-cart", [CartController::class, "showMyCart"]);
        Route::post("add-product", [CartController::class, "addProduct"]);
        Route::get("delete-product/{product_id}", [CartController::class, "deleteProduct"]);
    });

    Route::group(["prefix" => "orders"], function () {
        Route::post("checkout", [OrderController::class, "store"]);
    });
});
